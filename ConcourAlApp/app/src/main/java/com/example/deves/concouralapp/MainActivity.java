package com.example.deves.concouralapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b1 = (Button) findViewById(R.id.loginpatient);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, todo.class));
            }
        });
        Button b2 = (Button) findViewById(R.id.logincaretaker);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,caretaker.class));
            }
        });
        Button b3 = (Button) findViewById(R.id.sign);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,signup.class));
            }
        });
    }

}

/*
public void Agoal(View view) {
        a_goals++;
        displayForTeamA(a_goals,a_yellowcards,a_redcards);
    }
    Button b1 = (Button) findViewById(R.id.yourRewardsBtn);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, todo.class));
            }
        });
    Intent numbersIntent = new Intent(MainActivity.this, Basketball.class);
      star */